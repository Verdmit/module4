﻿using System;
using System.Globalization;
using static System.Console;

namespace M4
    {
    public class Module4
        {
        static void Main(string[] args)
            {

            #region Task4.1.A

            int _min, _max, _summ, _n1, _n2, _n3;
            int[] __arr, __arr2, __arr3, __arr0, __arr4, __arr5;
            string _userInput1, _userInput2;
            double _radius, _length, _square;
            var T1 = new Module4();

            __arr = new int[] { -1, 12, 23, 94, -65 };
            __arr2 = new int[] { 5, 19, 23, 4, 65 };
            __arr3 = new int[] { 0, 2, -5, -42, 20, 7, -2, -3 };
            __arr0 = new int[0];
            __arr4 = new int[] { -1, 12, 23, 94, -65 };
            __arr5 = new int[] { -1, -2, 0, 0 };

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Task 4.1.A");
            Console.ForegroundColor = ConsoleColor.Red;

            Console.WriteLine("Ищем максимальное число в массиве");
            Arr_2_Console_OUT(__arr);

            Console.ForegroundColor = ConsoleColor.Yellow;

            _min = T1.Task_1_B(__arr);
            _max = T1.Task_1_A(__arr);

            Console.Write("Маскимальный элемент массива = \t" + _max.ToString() + "\n");
            #endregion

            #region Task4.1.B
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\nTask 4.1.B");

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Ищем минимальный элемент в массиве");
            Console.ForegroundColor = ConsoleColor.Yellow;

            Console.Write("Минимальный элемент массива =\t" + _min.ToString() + "\n");
            Console.ReadKey();
            Console.Clear();
            #endregion

            #region Task4.1.C
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Task 4.1.C");
            Console.ForegroundColor = ConsoleColor.Red;
            Arr_2_Console_OUT(__arr);

            Console.WriteLine("Ищем сумму элементов в массиве. Ожидаем 63");
            _summ = T1.Task_1_C(__arr);

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Сумма элементов равна " + _summ.ToString() + "\n");
            __arr = new int[] { -1, -2, 0, 0 };
            Arr_2_Console_OUT(__arr);
            Print_Array_Changed();
            Console.WriteLine("Ожидаем -3");

            _summ = T1.Task_1_C(__arr);
            Console.ForegroundColor = ConsoleColor.Cyan;

            Console.Write("Сумма элементов равна " + _summ.ToString() + "\n");
            __arr = new int[] { -1, -2, -10, -10, -1 };
            Arr_2_Console_OUT(__arr);
            Print_Array_Changed();
            Console.WriteLine("Ожидаем -24");

            _summ = T1.Task_1_C(__arr);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Сумма элементов равна " + _summ.ToString() + "\n");

            __arr = new int[] { 0, 0 };
            Arr_2_Console_OUT(__arr);

            Print_Array_Changed();
            Console.WriteLine("Ожидаем 0");
            _summ = T1.Task_1_C(__arr);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Сумма элементов равна " + _summ.ToString() + "\n");

            __arr = new int[] { -10 };
            Arr_2_Console_OUT(__arr);

            Print_Array_Changed();
            Console.WriteLine("Ожидаем -10");
            _summ = T1.Task_1_C(__arr);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Сумма элементов равна " + _summ.ToString() + "\n");

            Console.WriteLine("Теперь кидаем херню на вход типа -new int[] { }-");
            _summ = T1.Task_1_C(new int[] { });
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Сумма элементов равна " + _summ.ToString() + "\n");

            Console.WriteLine("Снова кидаем херню на вход типа - null-");
            _summ = T1.Task_1_C(null);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Сумма элементов равна " + _summ.ToString() + "\n");
            Console.ReadKey();
            Console.Clear();
            #endregion

            #region Task4.1.D
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Task 4.1.D");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Ищем разность между Макс и Мин");

            Arr_2_Console_OUT(__arr);

            _min = T1.Task_1_B(__arr);
            _max = T1.Task_1_A(__arr);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Маскимальный элемент массива " + _max.ToString() + "\n");
            Console.Write("Минимальный элемент массива " + _min.ToString() + "\n");
            __arr = new int[] { -1, 12, 23, 94, -65 };

            Print_Array_Changed();

            Arr_2_Console_OUT(__arr);
            _min = T1.Task_1_B(__arr);
            _max = T1.Task_1_A(__arr);

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Маскимальный элемент массива " + _max.ToString() + "\n");
            Console.Write("Минимальный элемент массива " + _min.ToString() + "\n");

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Разность равна " + T1.Task_1_D(__arr).ToString() + "\n");
            Console.ReadKey();
            Console.Clear();
            #endregion

            #region Task4.1.E
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Task 4.1.E");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Увеличить четные элементы массива на максимальный элемент, \nнечётные уменьшить на минимальный элемент.");
            Console.ForegroundColor = ConsoleColor.Yellow;

            _max = T1.Task_1_A(__arr4);
            _min = T1.Task_1_B(__arr4);

            Console.WriteLine("У нас есть массив __arr4");
            Arr_2_Console_OUT(__arr4);
            Console.Write("Чётные увеличиваем на Макс " + _max.ToString() + "\n");
            Console.Write("Нечётные уменьшаем на Мин " + _min.ToString() + "\n\n");

            Console.WriteLine("Получилось следующее");
            Console.ForegroundColor = ConsoleColor.Cyan;
            T1.Task_1_E(__arr4);
            Arr_2_Console_OUT(__arr4);

            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("Task 4.1.E");

            _max = T1.Task_1_A(__arr4);
            _min = T1.Task_1_B(__arr4);

            Console.WriteLine("У нас есть массив __arr5");
            Arr_2_Console_OUT(__arr5);
            Console.Write("Чётные увеличиваем на Макс " + _max.ToString() + "\n");
            Console.Write("Нечётные уменьшаем на Мин " + _min.ToString() + "\n\n");

            Console.WriteLine("Получилось следующее. А ожидали -1, 0, 0, 2");
            Console.ForegroundColor = ConsoleColor.Cyan;
            T1.Task_1_E(__arr5);
            Arr_2_Console_OUT(__arr5);

            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("Task 4.1.E");


            Console.WriteLine("У нас есть 0 массив");
            Arr_2_Console_OUT(__arr0);

            Console.WriteLine("Посмотрим, что вышло");
            Console.ForegroundColor = ConsoleColor.Cyan;
            T1.Task_1_E(__arr0);

            #endregion

            #region Task4.2

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Task 4.2.A");
            Console.ForegroundColor = ConsoleColor.Red;

            Console.WriteLine("Создать набор методов позволяющих складывать между собой числа и массивы");
            Console.WriteLine("Сложим 2 натуральных числа");

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write("\nВведите число 1 -> ");
            Console.ForegroundColor = ConsoleColor.Magenta;

            InputNaturalNumberOnly(Console.ReadLine(), "Снова введите значение. Натуральное число", out _n1);

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write("\nВведите число 2 -> ");
            Console.ForegroundColor = ConsoleColor.Magenta;
            InputNaturalNumberOnly(Console.ReadLine(), "Снова введите значение. Натуральное число", out _n2);

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("\nЧисла сложили. Получилось " + T1.Task_2(_n1, _n2));

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write("\nВведите число 3 -> ");
            Console.ForegroundColor = ConsoleColor.Magenta;
            InputNaturalNumberOnly(Console.ReadLine(), "Снова введите значение. Натуральное число", out _n3);

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\nЧисла сложили. Получилось " + T1.Task_2(_n1, _n2, _n3) + "\n");

            Console.WriteLine("Сложим 3 вещественных числа");

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write("\nВведите число 1 -> ");
            Console.ForegroundColor = ConsoleColor.Magenta;


            InputDoubleNumberOnly(Console.ReadLine(), "Значение не может быть отрицательным или равно 0\nВведите натуральное число больше 0 -> ", out double _d1);
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write("\nВведите число 2 -> ");
            Console.ForegroundColor = ConsoleColor.Magenta;

            InputDoubleNumberOnly(Console.ReadLine(), "Значение не может быть отрицательным или равно 0\nВведите натуральное число больше 0 -> ", out double _d2);


            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write("\nВведите число 3 -> ");
            Console.ForegroundColor = ConsoleColor.Magenta;

            InputDoubleNumberOnly(Console.ReadLine(), "Значение не может быть отрицательным или равно 0\nВведите натуральное число больше 0 -> ", out double _d3);

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\nЧисла сложили. Получилось " + T1.Task_2(Convert.ToDouble(_d1), Convert.ToDouble(_d2), Convert.ToDouble(_d3)) + "\n");

            Console.WriteLine("Сложим 2 строки");

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write("\nВведите строку 1 -> ");
            Console.ForegroundColor = ConsoleColor.Magenta;
            _userInput1 = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write("\nВведите строку 2 -> ");
            Console.ForegroundColor = ConsoleColor.Magenta;
            _userInput2 = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\nСтроки сложили. Получилось " + T1.Task_2(_userInput1, _userInput2) + "\n");

            Console.ReadKey();
            Console.Clear();
            #endregion

            #region Task 4.2.E            
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Task 4.2.E ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Сложим массивы");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("1-й массив");
            Arr_2_Console_OUT(__arr);

            Console.WriteLine("2-й массив");
            Arr_2_Console_OUT(__arr2);

            Console.WriteLine("Сложим 2 массива одинаковой длины");
            Console.WriteLine("Получилось ");
            Arr_2_Console_OUT(T1.Task_2(__arr, __arr2));

            Console.WriteLine("1-й массив");
            Arr_2_Console_OUT(__arr);
            Console.WriteLine("2-й массив");
            Arr_2_Console_OUT(__arr3);
            Console.WriteLine("Сложим 2 массива разной длины");
            Console.WriteLine("Получилось ");
            Arr_2_Console_OUT(T1.Task_2(__arr, __arr3));
            #endregion

            #region Task 4.3 Создать набор методов с использованием ключевых слов ref и out
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Task 4.3.A");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Увеличиваем на 10 по ref ссылкам");
            Console.ForegroundColor = ConsoleColor.White;

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write("\nВведите число 1 -> ");
            Console.ForegroundColor = ConsoleColor.Magenta;
            InputNaturalNumberOnly(Console.ReadLine(), "Снова введите значение. Натуральное число", out _n1);

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write("\nВведите число 2 -> ");
            Console.ForegroundColor = ConsoleColor.Magenta;
            InputNaturalNumberOnly(Console.ReadLine(), "Снова введите значение. Натуральное число", out _n2);

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write("\nВведите число 3 -> ");
            Console.ForegroundColor = ConsoleColor.Magenta;
            InputNaturalNumberOnly(Console.ReadLine(), "Снова введите значение. Натуральное число", out _n3);

            T1.Task_3_A(ref _n1, ref _n2, ref _n3);

            Console.ForegroundColor = ConsoleColor.Magenta;

            Console.Write("\nЧисла увеличили. Получилось " + Convert.ToString(_n1) + " " + Convert.ToString(_n2) + " " + Convert.ToString(_n3) + "\n");
            Console.ReadKey();
            Console.Clear();

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Task 4.3.B");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Зная Радиус узнать Длину окружности и Площадь");
            Console.WriteLine("Методы имплементировать с использованием ключевых слов ref и out");

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write("\nВведите Радиус 1 -> ");
            Console.ForegroundColor = ConsoleColor.Magenta;
            InputDoubleNumberOnly(Console.ReadLine(), "Радиус не может быть отрицательным или равен 0\nВведите натуральное число больше 0 -> ", out _radius);
            //_userInput1 = Console.ReadLine();
            //_radius =Convert.ToDouble(_userInput1);

            WriteLine("У нас есть 3 переменных: " + "Радиус=" + _radius + " Длина=" + (_length = 0) + " Площадь=" + (_square = 0));

            T1.Task_3_B(_radius, out _length, out _square);
            Console.ForegroundColor = ConsoleColor.Cyan;
            WriteLine("Наш метод выдал, что в массиве c радиусом =" + _radius + "\nДлина=" + _length + "\nПлощадь=" + _square);
            Console.ReadKey();
            Console.Clear();

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Task 4.3.C");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Найти минимальный и максимальный элемент массива и сумму всех элементов массива.");
            Console.ForegroundColor = ConsoleColor.White;

            WriteLine("У нас есть 3 переменных " + "Мин=" + (_min = 0) + " Макс=" + (_max = 0) + " Сумма элементов=" + (_summ = 0));
            Arr_2_Console_OUT(__arr);
            T1.Task_3_C(__arr, out _min, out _max, out _summ);
            Console.ForegroundColor = ConsoleColor.Cyan;
            WriteLine("Наш метод выдал, что в массиве " + "Мин=" + _min + " Макс=" + _max + " Сумма элементов=" + _summ);

            Console.ReadKey();
            Console.Clear();
            #endregion

            #region Task 4
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Task 4.4.A");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("С помощью кортежей увеличивать значение трех входных переменных на 10");
            Console.ForegroundColor = ConsoleColor.White;

            Write("Введите значение 1 ->");
            InputNaturalNumberOnly(Console.ReadLine(), "Снова введите значение. Натуральное число", out _n1);

            Write("Введите значение 2 ->");
            InputNaturalNumberOnly(Console.ReadLine(), "Снова введите значение. Натуральное число", out _n2);

            Write("Введите значение 3 ->");
            InputNaturalNumberOnly(Console.ReadLine(), "Снова введите значение. Натуральное число", out _n3);

            WriteLine("Имеем на входе следующее: \t" + " " + _n1 + " " + _n2 + " " + _n3);

            var _tupIn = (_n1, _n2, _n3);

            var _tupOut = T1.Task_4_A(_tupIn);

            Write("Выводим кортеж выходной  \t");
            Write(_tupOut.Item1 + " " + _tupOut.Item2 + " " + _tupOut.Item3 + "\n");

            Console.ReadKey();
            Console.Clear();

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Task 4.4.B");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("С помощью кортежей найти длину окружности и площадь круга");
            Console.ForegroundColor = ConsoleColor.White;

            Write("Введите радиус ->");
            InputNaturalNumberOnly(Console.ReadLine(), "Снова введите значение. Натуральное число", out _n1);

            var _tupOutR = T1.Task_4_B(_n1);

            Write("Выводим кортеж выходной  \t");
            Write("Длина = " + _tupOutR.Item1 + " Площадь" + _tupOutR.Item2 + "\n");

            Console.ReadKey();
            Console.Clear();

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Task 4.4.C");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("С помощью кортежей найти минимальный и максимальный элемент массива и сумму всех элементов массива");
            Console.ForegroundColor = ConsoleColor.White;

            WriteLine("Имеем на входе следующее:");
            Arr_2_Console_OUT(__arr);

            _tupOut = T1.Task_4_C(__arr);

            Write("Выводим кортеж выходной  \t");
            Write("Мин " + _tupOut.Item1 + " Макс " + _tupOut.Item2 + " Сумма " + _tupOut.Item3 + "\n");

            T1.Task_4_C(__arr);

            Console.ReadKey();
            Console.Clear();
            #endregion

            #region Task 4.5
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Task 4.5 ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Создать метод, увеличивающий каждый элемент массива на 5.");
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("Есть массив.");
            Arr_2_Console_OUT(__arr2);
            Console.WriteLine("Увеличим все элементы на 5");
            T1.Task_5(__arr2);
            Console.WriteLine("Увеличили. Вышло:");
            Arr_2_Console_OUT(__arr2);

            Console.ReadKey();
            Console.Clear();
            #endregion

            #region Task 4.6
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Task 4.6 ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Отсортировать массив.  Передать в виде параметра направление сортировки.");
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("Есть массив.");
            Arr_2_Console_OUT(__arr2);
            Console.WriteLine("Сортирнём по восходящей");
            T1.Task_6(__arr2, SortDirection.Ascending);
            Console.WriteLine("Сортирнули. Вышло:");
            Arr_2_Console_OUT(__arr2);
            Console.WriteLine("Сортирнём по нисходящей");
            T1.Task_6(__arr2, SortDirection.Descending);
            Console.WriteLine("Сортирнули. Вышло:");
            Arr_2_Console_OUT(__arr2);

            Console.ReadKey();
            Console.Clear();
            #endregion

            #region Task 4.7
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Task 4.7 ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Решить уравнение методом деления отрезка пополам, используя рекурсию.");
            Console.ForegroundColor = ConsoleColor.White;


            Console.ReadKey();
            Console.Clear();
            #endregion



            Console.ResetColor();

            #region Мои методы для проверки Майна
            static void Arr_2_Console_OUT(int[] aRR)
                {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("Существующий массив ");
                Console.ForegroundColor = ConsoleColor.Green;

                try
                    {
                    foreach ( var vallu in aRR )
                        {
                        Console.Write($"{vallu} ");
                        //Console.Write(vallu + " ");
                        }
                    Console.Write("\n");
                    Console.ForegroundColor = ConsoleColor.White;
                    }

                catch
                    {
                    Console.WriteLine("Массив не инициализирован");
                    }
                }


            static void InputNaturalNumberOnly(string data, string errorMessage, out int value)
                {
                while ( (!int.TryParse(data, out value)) || (value < 0) )
                    {
                    Console.WriteLine(errorMessage);
                    data = Console.ReadLine();
                    }
                }

            static void InputDoubleNumberOnly(string data, string errorMessage, out double value)
                {
                while ( (!double.TryParse(data, out value)) || (value <= 0) )
                    {
                    Console.Write(errorMessage);
                    data = Console.ReadLine();
                    }
                Console.WriteLine("\n");
                }

            static void Print_Array_Changed( )
                {
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.WriteLine("Заменили массив");
                Console.ForegroundColor = ConsoleColor.Yellow;
                }

            #endregion
            }
        #region Таски 
        public int Task_1_A(int[] array)
            {
            /// <summary>
            /// A. Поиск Максимального элемента в массиве.
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>

            try
                {
                if ( array == null )
                    {
                    return 0;
                    }

                int maxInArr = array[0];

                for ( int a = 1; a < array.Length; a++ )
                    {
                    if ( maxInArr < array[a] )
                        {
                        maxInArr = array[a];
                        }
                    }
                return maxInArr;
                }

            catch ( System.IndexOutOfRangeException )
                {
                throw new ArgumentOutOfRangeException();
                }
            catch ( System.NullReferenceException )
                {
                throw new ArgumentNullException();
                }
            }

        public int Task_1_B(int[] array)
            {
            /// <summary>
            /// B.Поиск Минимального элемента в массиве.
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>

            try
                {
                if ( array == null )
                    {
                    return 0;
                    }

                int minnArr = array[0];
                for ( int a = 1; a < array.Length; a++ )
                    {
                    if ( minnArr > array[a] )
                        {
                        minnArr = array[a];
                        }
                    }
                return minnArr;
                }

            catch ( System.IndexOutOfRangeException )
                {
                throw new ArgumentOutOfRangeException();
                }
            catch ( System.NullReferenceException )
                {
                throw new ArgumentNullException();
                }
            }

        public int Task_1_C(int[] array)
            {
            /// <summary>
            /// C. Сумму всех элементов в массиве.
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>

            try
                {
                if ( array == null )
                    {
                    return 0;
                    }

                int summArr = array[0];
                for ( int a = 1; a < array.Length; a++ )
                    {
                    summArr += array[a];
                    }
                return summArr;
                }

            catch ( System.IndexOutOfRangeException )
                {
                throw new ArgumentOutOfRangeException();
                }
            catch ( System.NullReferenceException )
                {
                throw new ArgumentNullException();
                }


            }

        public int Task_1_D(int[] array)
            {
            /// <summary>
            /// D. Разность между максимальным и минимальным элементом.
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>

            int maxInArr = array[0];
            int minInArr = array[0];

            for ( int a = 1; a < array.Length; a++ )
                {
                if ( maxInArr < array[a] )
                    {
                    maxInArr = array[a];
                    }

                if ( minInArr > array[a] )
                    {
                    minInArr = array[a];
                    }
                }
            return maxInArr - minInArr;
            }

        public void Task_1_E(int[] array)
            {


            try
                {
                int maxInArr = array[0];
            int minInArr = array[0];

            for ( int a = 1; a < array.Length; a++ )
                {
                if ( maxInArr < array[a] )
                    {
                    maxInArr = array[a];
                    }

                if ( minInArr > array[a] )
                    {
                    minInArr = array[a];
                    }
                }

            

            for ( int evenOdd = 0; evenOdd < array.Length; evenOdd++ )
                {
                if ( evenOdd % 2 == 0 )
                    {
                    array[evenOdd] = maxInArr + array[evenOdd];
                    }
                else
                    {
                    array[evenOdd] -= minInArr;
                    }
                }
            return;
                }
            catch ( System.IndexOutOfRangeException )
                {
                throw new ArgumentOutOfRangeException();
                }
            catch ( System.NullReferenceException )
                {
                throw new ArgumentNullException();
                }
            }

        public int Task_2(int a, int b, int c)
            {/// +++Три целых числа
            try
                {
                return a + b + c;
                }
            catch ( System.IndexOutOfRangeException )
                {
                throw new ArgumentOutOfRangeException();
                }
            catch ( System.NullReferenceException )
                {
                throw new ArgumentNullException();
                }
            }

        public int Task_2(int a, int b)
            {/// +++* B. Два целых числа
            try
                {
                return a + b;
                }
            catch ( System.IndexOutOfRangeException )
                {
                throw new ArgumentOutOfRangeException();
                }
            catch ( System.NullReferenceException )
                {
                throw new ArgumentNullException();
                }
            }

        public double Task_2(double a, double b, double c)
            {/// +++ Три дробных числа
            try
                {
                return a + b + c;
                }
            catch ( System.IndexOutOfRangeException )
                {
                throw new ArgumentOutOfRangeException();
                }
            catch ( System.NullReferenceException )
                {
                throw new ArgumentNullException();
                }
            }

        public string Task_2(string a, string b)
            {/// +++* D.  Две строки (результатом должна быть конкатенация строк)
            return a + b;
            }

        public int[] Task_2(int[] a, int[] b)
            {
            /// <summary>
            /// *E складывать между собой Два массива одинаковой (как одинаковой, так и разной длины) 
            /// Результатом будет сумма a[i] + b[i].
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>
            try
                {
                int[] result;
                if ( a.Length == b.Length )
                    {
                    result = new int[b.Length];
                    for ( int index = 0; index < b.Length; index++ )
                        {
                        result[index] = a[index] + b[index];
                        }
                    }
                else if ( a.Length > b.Length )
                    {
                    result = new int[a.Length];
                    for ( int index = 0; index < b.Length; index++ )
                        {
                        result[index] = a[index] + b[index];
                        }

                    for ( int index = b.Length; index < a.Length; index++ )
                        {
                        result[index] = a[index];
                        }
                    }
                else
                    {
                    result = new int[b.Length];
                    for ( int index = 0; index < a.Length; index++ )
                        {
                        result[index] = a[index] + b[index];
                        }

                    for ( int index = a.Length; index < b.Length; index++ )
                        {
                        result[index] = b[index];
                        }
                    }

                return result;
                }
            catch ( System.IndexOutOfRangeException )
                {
                throw new ArgumentOutOfRangeException();
                }
            catch ( System.NullReferenceException )
                {
                throw new ArgumentNullException();
                }
            }

        #endregion
        public void Task_3_A(ref int a, ref int b, ref int c)
            {
            /// <summary>
            /// с использованием ключевых слов ref и out
            /// A. увеличивать значение трех входных переменных на 10.
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>
            try
                {
                a += 10;
                b += 10;
                c += 10;
                }
            catch ( System.IndexOutOfRangeException )
                {
                throw new ArgumentOutOfRangeException();
                }
            catch ( System.NullReferenceException )
                {
                throw new ArgumentNullException();
                }

            }

        public void Task_3_B(double radius, out double length, out double square)
            {
            /// <summary>
            /// с использованием ключевых слов ref и out
            /// B. находить длину окружности и площадь круга.
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>

            

            if ( radius <= 0 )
                {
                length = 0;
                square = 0;
                }
            else
                {
                length = 2 * Math.PI * radius;
                square = Math.PI * Math.Pow(radius, 2);
                }
            }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
            {
            /// <summary>
            /// с использованием ключевых слов ref и out
            /// находить минимальный и максимальный элемент массива и сумму всех элементов массива.
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>

            try
                {
                maxItem = minItem = sumOfItems = array[0];

                for ( int a = 1; a < array.Length; a++ )
                    {
                    sumOfItems += array[a];
                    if ( maxItem < array[a] )
                        {
                        maxItem = array[a];
                        }
                    else if ( minItem > array[a] )
                        {
                        minItem = array[a];
                        }
                    }
                }
            catch ( System.IndexOutOfRangeException )
                {
                throw new ArgumentOutOfRangeException();
                }
            catch ( System.NullReferenceException )
                {
                throw new ArgumentNullException();
                }
            }

        public (int, int, int) Task_4_A((int, int, int) numbers)
            {
            /// <summary>
            /// с использованием кортежей увеличить значение трех входных переменных на 10.
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>

            try
                {
                numbers.Item1 += 10;
                numbers.Item2 += 10;
                numbers.Item3 += 10;
                return numbers;
                }

            catch ( System.IndexOutOfRangeException )
                {
                throw new ArgumentOutOfRangeException();
                }
            catch ( System.NullReferenceException )
                {
                throw new ArgumentNullException();
                }
            }

        public (double, double) Task_4_B(double radius)
            {
            /// <summary>
            /// с использованием кортежей
            /// Позволяющий находить длину окружности и площадь круга.
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>
            

            try
                {
                if ( radius <= 0 )
                    {
                    throw new ArgumentOutOfRangeException();
                    }
                else
                    {
                    var tup4Ret = (2 * Math.PI * radius, Math.PI * Math.Pow(radius, 2));
                    return tup4Ret;
                    }
                }
            catch ( System.IndexOutOfRangeException )
                {
                throw new ArgumentOutOfRangeException();
                }
            catch ( System.NullReferenceException )
                {
                throw new ArgumentNullException();
                }

            }

        public (int, int, int) Task_4_C(int[] array)
            {
            /// <summary>
            /// с использованием кортежей
            /// Позволяющий находить минимальный и максимальный элемент массива и сумму всех элементов массива.
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>
            var tupRet = (0, 0, 0);

            try
                {
                if ( array == null )
                    {
                    tupRet = (0, 0, 0);
                    return tupRet;
                    }

                int maxInArr = array[0];

                for ( int a = 1; a < array.Length; a++ )
                    {
                    if ( maxInArr < array[a] )
                        {
                        maxInArr = array[a];
                        }
                    }

                tupRet.Item2 = maxInArr;

                int minnArr = array[0];
                for ( int a = 1; a < array.Length; a++ )
                    {
                    if ( minnArr > array[a] )
                        {
                        minnArr = array[a];
                        }
                    }
                tupRet.Item1 = minnArr;

                int summArr = array[0];
                for ( int a = 1; a < array.Length; a++ )
                    {
                    summArr += array[a];
                    }
                tupRet.Item3 = summArr;

                return tupRet;
                }

            catch ( System.IndexOutOfRangeException )
                {
                throw new ArgumentOutOfRangeException();
                }
            catch ( System.NullReferenceException )
                {
                throw new ArgumentNullException();
                }
            }

        public void Task_5(int[] array)
            {
            /// <summary>
            /// Создать метод, увеличивающий каждый элемент массива на 5.
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>
            try
                {
                for ( int aRRind = 0; aRRind < array.Length; aRRind++ )
                    {
                    array[aRRind] += 5;
                    }
                return;
                }
            catch ( System.IndexOutOfRangeException )
                {
                throw new ArgumentOutOfRangeException();
                }
            catch ( System.NullReferenceException )
                {
                throw new ArgumentNullException();
                }
            }

        public void Task_6(int[] array, SortDirection direction)
            {
            /// <summary>
            /// Отсортировать массив. Передать в виде параметра направление сортировки.
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>

                        try
                            {
                            if ( direction == SortDirection.Ascending )
                                {
                                Array.Sort(array);
                                }
                            else if ( direction == SortDirection.Descending )
                                {
                                Array.Reverse(array);
                                }
                            return;
                            }
            catch ( System.IndexOutOfRangeException )
                {
                throw new ArgumentOutOfRangeException();
                }
            catch ( System.NullReferenceException )
                {
                throw new ArgumentNullException();
                }
            }

        public double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
            {
            /// <summary>
            ///Решить уравнение методом деления отрезка пополам, используя рекурсию.
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>

            result = (x1 + x2) / 2;
            if ( func(result) >= 0 )
                {
                x1 = result;
                }
            else
                {
                x2 = result;
                }

            if ( Math.Abs(x1 - x2) <= e )
                {

                return result;
                }
            else
                {
                return Task_7(func, x1, x2, result);
                }
            }
        }
    }

